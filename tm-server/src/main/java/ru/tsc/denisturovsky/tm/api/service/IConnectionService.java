package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IConnectionService {

    void close();

    @NotNull
    EntityManager getEntityManager();

}