package ru.tsc.denisturovsky.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
