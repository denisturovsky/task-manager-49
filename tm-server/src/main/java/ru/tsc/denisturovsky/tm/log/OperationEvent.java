package ru.tsc.denisturovsky.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    private Object entity;

    private String table;

    private long timestamp = System.currentTimeMillis();

    private OperationType type;

    public OperationEvent(
            final OperationType type,
            final Object entity
    ) {
        this.type = type;
        this.entity = entity;
    }

}
