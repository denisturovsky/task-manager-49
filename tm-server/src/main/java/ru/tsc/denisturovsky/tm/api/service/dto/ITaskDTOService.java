package ru.tsc.denisturovsky.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.enumerated.Status;

import java.util.Date;
import java.util.List;

public interface ITaskDTOService extends IUserOwnedDTOService<TaskDTO> {

    void changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    ) throws Exception;

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name
    ) throws Exception;

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws Exception;

    void updateOneById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateProjectIdById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String projectId
    ) throws Exception;

}
