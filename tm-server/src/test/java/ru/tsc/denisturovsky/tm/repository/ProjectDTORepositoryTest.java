package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.comparator.NameComparator;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.repository.dto.ProjectDTORepository;
import ru.tsc.denisturovsky.tm.sevice.ConnectionService;
import ru.tsc.denisturovsky.tm.sevice.PropertyService;
import ru.tsc.denisturovsky.tm.sevice.dto.ProjectDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.TaskDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectDTORepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private static IProjectDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepository(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return CONNECTION_SERVICE.getEntityManager();
    }

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @Test
    public void addByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(USER_ID, USER_PROJECT3));
            entityManager.getTransaction().commit();
            @Nullable final ProjectDTO project = repository.findOneById(USER_ID, USER_PROJECT3.getId());
            Assert.assertNotNull(project);
            Assert.assertEquals(USER_PROJECT3.getId(), project.getId());
            Assert.assertEquals(USER_ID, project.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(USER_ID);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Before
    public void before() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER_ID, USER_PROJECT1);
            repository.add(USER_ID, USER_PROJECT2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(USER_ID);
            entityManager.getTransaction().commit();
            Assert.assertEquals(0, repository.getSize(USER_ID));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTO project = repository.create(USER_ID, USER_PROJECT3.getName());
            Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
            Assert.assertEquals(USER_ID, project.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserIdWithDescription() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTO project = repository.create(USER_ID, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
            Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
            Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
            Assert.assertEquals(USER_ID, project.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(USER_ID, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_ID, USER_PROJECT1.getId()));
        entityManager.close();
    }

    @Test
    public void findAllByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<ProjectDTO> projects = repository.findAll(USER_ID);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(USER_ID, project.getUserId()));
        entityManager.close();
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<ProjectDTO> projects = repository.findAll(USER_ID, comparator);
        Assert.assertNotNull(projects);
        projects.forEach(project -> Assert.assertEquals(USER_ID, project.getUserId()));
        entityManager.close();
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(USER_ID, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = repository.findOneById(USER_ID, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
        entityManager.close();
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(2, repository.getSize(USER_ID));
        entityManager.close();
    }

    @Test
    public void removeByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(USER_ID, USER_PROJECT2);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER_PROJECT2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void update() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            USER_PROJECT1.setName(USER_PROJECT3.getName());
            repository.update(USER_PROJECT1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_PROJECT3.getName(), repository.findOneById(USER_ID, USER_PROJECT1.getId()).getName());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}