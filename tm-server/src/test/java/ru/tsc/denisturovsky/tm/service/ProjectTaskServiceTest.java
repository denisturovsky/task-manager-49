package ru.tsc.denisturovsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.denisturovsky.tm.exception.entity.TaskNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.TaskIdEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.UserIdEmptyException;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.sevice.ConnectionService;
import ru.tsc.denisturovsky.tm.sevice.PropertyService;
import ru.tsc.denisturovsky.tm.sevice.dto.ProjectDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.ProjectTaskDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.TaskDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.UserDTOService;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.TaskTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final IProjectTaskDTOService service = new ProjectTaskDTOService(PROJECT_SERVICE, TASK_SERVICE);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @After
    public void after() throws Exception {
        TASK_SERVICE.clear(USER_ID);
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Before
    public void before() throws Exception {
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT1);
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT2);
        TASK_SERVICE.add(USER_ID, USER_TASK1);
        TASK_SERVICE.add(USER_ID, USER_TASK2);
    }

    @Test
    public void bindTaskToProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(USER_ID, null, USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(USER_ID, "", USER_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.bindTaskToProject(USER_ID, NON_EXISTING_PROJECT_ID, USER_TASK1.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID));
        service.bindTaskToProject(USER_ID, USER_PROJECT2.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
        service.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById(null, USER_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById("", USER_PROJECT1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(USER_ID, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(USER_ID, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeProjectById(USER_ID, NON_EXISTING_PROJECT_ID));
        service.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
        service.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK2.getId());
        service.removeProjectById(USER_ID, USER_PROJECT1.getId());
        Assert.assertNull(PROJECT_SERVICE.findOneById(USER_ID, USER_PROJECT1.getId()));
        Assert.assertNull(TASK_SERVICE.findOneById(USER_ID, USER_TASK1.getId()));
        Assert.assertNull(TASK_SERVICE.findOneById(USER_ID, USER_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(USER_ID, null, USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(USER_ID, "", USER_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.unbindTaskFromProject(USER_ID, NON_EXISTING_PROJECT_ID, USER_TASK1.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID));
        service.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
        service.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

}