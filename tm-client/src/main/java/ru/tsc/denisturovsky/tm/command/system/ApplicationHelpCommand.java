package ru.tsc.denisturovsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Locale;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Show terminal commands list";

    @NotNull
    public static final String NAME = "help";

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        commands.forEach(System.out::println);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
